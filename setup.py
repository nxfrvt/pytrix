import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pytrix",
    version="0.1.1",
    author="Bartłomiej Mazurek",
    author_email="mazurekb02@gmail.com",
    description="Module for matrices",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/nxfrvt/card_game_module",
    packages=setuptools.find_packages(),
    classifiers=[
        "Development Status :: 1 - Planning",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',

)
