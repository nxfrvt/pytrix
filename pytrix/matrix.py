#!/usr/bin/env python3

__author__ = "Bartłomiej Mazurek"
__copyright__ = "Copyright (c) 2020 Bartłomiej Mazurek"
__email__ = "mazurekb02@gmail.com"
__version__ = "0.0.1"


class Matrix:
    """class handling creation of matrices and their being"""

    def __init__(self, matrix: []):
        """Ctor

        :param matrix: list from which matrix will be created"""

        self.matrix: [] = matrix

        if type(self.matrix[0]) != list:
            self.rows = 1
            self.columns = 1
        else:
            self.rows = len(self.matrix)
            self.columns = len(self.matrix[0])

        if not self.is_matrix():
            self.matrix = None

    def is_matrix(self):
        """Checks if matrix meets minimum requirements to be a matrix"""
        return False if self.rows * self.columns < 2 else True

    def show_matrix(self):
        """Print matrix row by row"""
        for row in self.matrix:
            print(row)

    def get_value(self, row: int, column: int):
        """Allows to get value from specified
        position in the matrix

        :param row:number of row from which we are getting the value
        :param column: number of column from which we are getting the value
        :return value from specified position"""
        return self.matrix[row][column]

    def set_value(self, row: int, column: int, value: int):
        """Sets value in the matrix at specified position

        :param row: number of row in which we are setting the value
        :param column: number of column in which we are setting the value
        :param value: specified value to set in the matrix"""
        self.matrix[row][column] = value

    def __eq__(self, other):
        """This checks if matrices are of the same size"""
        if (self.rows == other.rows
                and self.columns == other.columns):
            return True
        else:
            return False

    def add(self, other):
        """adds other matrix to the first one.
        Matrices have to be of the same size"""
        if self == other:
            for row in range(0, self.rows):
                for column in range(0, self.columns):
                    result = (self.get_value(row, column)
                              + other.get_value(row, column))
                    self.set_value(row, column, result)

    def multiply_by_scalar(self, scalar: int):
        """Multiplies matrix by specified scalar
        :param scalar value which we multiply matrix by"""
        for row in range(0, self.rows):
            for column in range(0, self.columns):
                self.set_value(row, column,
                               self.get_value(row, column) * scalar)

    def multiply_by(self, other):
        """Multiply the matrix by another matrix.
        For this to be possible, the rows of
         the matrix must be in the same number as
          the columns of the second."""
        if self.columns == other.rows:
            temp_self_matrix = Matrix(self.matrix)

            self.matrix = []
            self.rows = temp_self_matrix.rows
            self.columns = other.columns

            for row in range(0, self.rows):
                self.matrix.append([])
                for column in range(0, self.columns):
                    value = 0
                    for other_row in range(0, other.rows):
                        value += (
                            (temp_self_matrix.get_value(row, other_row)
                             * other.get_value(other_row, column))
                        )
                    self.matrix[row].append(value)

    def transpose(self):
        """Transposes the matrix"""
        temp_matrix = Matrix(self.matrix)
        self.matrix = []
        self.columns = temp_matrix.rows
        self.rows = temp_matrix.columns

        for row in range(0, self.rows):
            self.matrix.append([])
            for column in range(0, self.columns):
                value = temp_matrix.get_value(column, row)
                self.matrix[row].append(value)

    def find_determinant(self):
        """Finds the determinant of the matrix.
        If determinant can't be found returns None

        :return determinant of the matrix"""
        found = False
        determinant = None
        while not found:
            if self.rows == self.columns:
                level = self.rows
                if level == 2:
                    determinant = self.det_2x2()
                    found = True
                elif level == 3:
                    determinant = self.det_3x3()
                    found = True
                else:
                    # TODO
                    self.la_place_ext(level)
                    found = True
            else:
                found = True
        return determinant

    def det_2x2(self):
        """Finding determinant of a 2d matrix.
        If matrix in not 2d, returns None
        :return determinant"""
        if self.rows == 2 and self.columns == 2:
            determinant = (self.get_value(0, 0) * self.get_value(1, 1)
                           - self.get_value(0, 1) * self.get_value(1, 0))
            return determinant
        else:
            return None

    def det_3x3(self):
        """Implementation of finding determinant
        with a Saurus method. If matrix is not 3d,
        returns None
        :return determinant"""
        if self.rows == 3 and self.columns == 3:
            determinant = (self.get_value(0, 0) * self.get_value(1, 1) * self.get_value(2, 2)
                           + self.get_value(0, 1) * self.get_value(1, 2) * self.get_value(2, 0)
                           + self.get_value(0, 2) * self.get_value(1, 0) * self.get_value(2, 1)
                           - self.get_value(0, 2) * self.get_value(1, 1) * self.get_value(2, 0)
                           - self.get_value(0, 0) * self.get_value(1, 2) * self.get_value(2, 1)
                           - self.get_value(0, 1) * self.get_value(1, 0) * self.get_value(2, 2))
            return determinant
        else:
            return None

    def la_place_ext(self, level: int):
        # TODO
        pass
