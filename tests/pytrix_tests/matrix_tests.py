#!/usr/bin/env python3

__author__ = "Bartłomiej Mazurek"
__copyright__ = "Copyright (c) 2020 Bartłomiej Mazurek"
__email__ = "mazurekb02@gmail.com"
__version__ = "0.0.1"

import unittest
from pytrix.matrix import Matrix as mx


class BadDataTest(unittest.TestCase):
    def test_bad_data(self):
        self.incorrect_data = [1]
        self.incorrect_matrix = mx(self.incorrect_data)
        self.assertEqual(self.incorrect_matrix.matrix, None, "Matrix expected to be None, "
                                                             "found {} instead".format(self.incorrect_matrix.matrix))
        self.data = [[0, 1, 2],
                     [3, 4, 5]]
        self.matrix = mx(self.data)
        self.determinant = self.matrix.det_2x2()


class DeterminantTest(unittest.TestCase):
    def test_cant_find(self):
        self.data = [[0, 1, 2],
                     [3, 4, 5]]
        self.matrix = mx(self.data)
        self.determinant = self.matrix.find_determinant()
        self.assertEqual(None, self.determinant, f"Expected None, found {self.determinant} instead")

    def test_not_2x2(self):
        self.data = [[0, 1, 2],
                     [3, 4, 5]]
        self.matrix = mx(self.data)
        self.determinant = self.matrix.det_2x2()
        self.assertEqual(None, self.determinant, "Possibility of calculation not expected")

    def test_2x2(self):
        self.data = [[2, 2],
                     [4, 4]]
        self.matrix = mx(self.data)
        self.determinant = self.matrix.find_determinant()
        self.assertEqual(0, self.determinant, f"Expected 0, found {self.determinant} instead")

    def test_not_3x3(self):
        self.data = [[0, 2, 2],
                     [2, 0, 2]]
        self.matrix = mx(self.data)
        self.determinant = self.matrix.det_3x3()
        self.assertEqual(None, self.determinant, f"Expected None, found {self.determinant} instead")

    def test_3x3(self):
        self.data = [[-5, 0, -1],
                     [1, 2, -1],
                     [-3, 4, 1]]
        self.matrix = mx(self.data)
        self.determinant = self.matrix.find_determinant()
        self.assertEqual(-40, self.determinant, f"Expected -40, found {self.determinant} instead")

    def test_4x4(self):
        self.data = [[1, 2, 3, 4],
                     [5, 6, 7, 8],
                     [9, 10, 11, 12],
                     [13, 14, 15, 16]]
        self.matrix = mx(self.data)
        self.determinant = self.matrix.find_determinant()
        self.assertEqual(None, self.determinant, f"Expected None, found {self.determinant} instead")


class MultiplyingTest(unittest.TestCase):
    def test_multiply_scalar(self):
        self.data = [[1, 2],
                     [3, 4]]
        self.matrix = mx(self.data)
        self.matrix.multiply_by_scalar(2)
        self.assertEqual(4, self.matrix.get_value(0, 1), f"Expected 2, found {self.matrix.get_value(0, 1)} instead")

    def test_multiply_by(self):
        self.data = [[2, 3],
                     [5, 6],
                     [8, 9]]
        self.matrix = mx(self.data)
        self.matrix_2 = mx(self.data)
        self.matrix_2.transpose()
        self.matrix.multiply_by(self.matrix_2)
        self.assertEqual(61, self.matrix.get_value(1, 1), f"Expected 61, found {self.matrix.get_value(2, 2)} instead")


class AdditionTest(unittest.TestCase):
    def test_add(self):
        self.data = [[2, 3],
                     [3, 5]]
        self.matrix = mx(self.data)
        self.matrix_2 = mx(self.data)
        self.matrix_2.transpose()
        self.matrix.add(self.matrix_2)
        self.assertEqual(4, self.matrix.get_value(0, 0), f"Expected 4, found {self.matrix.get_value(0, 0)} instead")

    def test_bad_add(self):
        self.data = [[2, 3],
                     [4, 1]]
        self.data_2 = [[2, 1, 1],
                       [3, 3, 3]]
        self.matrix = mx(self.data)
        self.matrix_2 = mx(self.data_2)
        self.matrix.add(self.matrix)


class VoidTest(unittest.TestCase):
    def test_show(self):
        self.data = [[1, 1],
                     [1, 1]]
        self.matrix = mx(self.data)
        self.matrix.show_matrix()

    def test_not_equal(self):
        self.data = [[2],
                     [1]]
        self.matrix = mx(self.data)
        self.other_matrix = mx(self.data)
        self.other_matrix.transpose()
        equal = (self.matrix == self.other_matrix)
        self.assertEqual(False, equal, f"Expected False, got {equal} instead")


if __name__ == "__main__":
    unittest.main()
