#!/usr/bin/env python3

__author__ = "Bartłomiej Mazurek"
__copyright__ = "Copyright (c) 2020 Bartłomiej Mazurek"
__email__ = "mazurekb02@gmail.com"
__version__ = "0.0.1"

import unittest
from tests.pytrix_tests.matrix_tests import DeterminantTest
from tests.pytrix_tests.matrix_tests import MultiplyingTest
from tests.pytrix_tests.matrix_tests import BadDataTest
from tests.pytrix_tests.matrix_tests import AdditionTest
from tests.pytrix_tests.matrix_tests import VoidTest

if __name__ == "__main__":
    unittest.main()
