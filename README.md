# Python module for matrices [![pipeline status][pipeline]][master] [![coverage report][coverage]][master]

## Table of contents
* [General info](#general-info)
* [Authors](#authors)

## General info

Simple module for performing 
basic operations on matrices in Python

## Authors

![alt text][logo]
**Bartłomiej Mazurek**
========================

See also the list of [contributors][contributors] who participated in this project.

[contributors]: https://gitlab.com/nxfrvt/pytrix/-/graphs/master
[logo]: https://avatars0.githubusercontent.com/u/64803053?s=460&u=2009c05a55d5d25adb8264f8f6c419daed7e3bdb&v=4
[pipeline]: https://gitlab.com/nxfrvt/pytrix/badges/master/pipeline.svg
[coverage]: https://gitlab.com/nxfrvt/pytrix/badges/master/coverage.svg
[master]: https://gitlab.com/nxfrvt/pytrix/-/commits/master
